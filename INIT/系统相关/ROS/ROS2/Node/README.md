# ROS2 创建一个简单的节点
## 包含订阅者和发布者
> ## 1.创建功能包
> ### 首先打开一个新终端，并且设置环境变量，运行创建功能包指令
> ```
> ros2 pkg create --build-type ament_cmake pubsub_node
> ```
> ### 终端将返回一些信息，代表功能包创建成功
> ### 可以进入创建出的`pubsub_node` 包(文件夹)：
> ### 文件夹：`include`和`src`
> ### 文件：`package.xml`和`CMakeLists.txt`
> ### 两个文件是必须要有的，文件夹可以更改，按自己的想法来
---
> ## 2.编写发布者
> ### 在功能包文件下创建一个cpp文件
> ### 打开终端`touch test.cpp`
>> ### 顶部(头文件，命名空间)
>> ```cpp
>> #include <chrono>
>> #include <functional>
>> #include <memory>
>> #include <string>
>> //ros
>> #include <rclcpp/rclcpp.hpp>
>> #include <std_msgs/msg/string.hpp>
>> using namespace std::chrono_literals;
>> ```
>> ### 下面通过继承rclcpp::Node节点基类创建节点类MinimalPublisher
>> ### 代码中的每个this都引用节点
>> ```cpp
>> class MinimalPublisher : public rclcpp::Node
>> ```
>> ### 接下来是节点类MinimalPublisher的构造函数
>> ### 将`count_`变量初始化为`0`,节点名初始化为`"minimal_publisher"`。构造函数内先是创建一个发布者，话题名是`topic`,话题消息是`String`，保存消息队列长度为`10`,然后创建一个定时器`timer_`，做了一个`500ms`的定时，每次触发定时器后都会运行回调函数`timer_callback`。
>> ```cpp
>> public:
>>  MinimalPublisher() : Node("minimal_publisher"),count_(0)
>>  {
>>      publisher_ = this->create_publisher<std_msgs::msg::String>("topic",10);
>>      timeer_ = this->create_wall_timer(500ms,std::bind(&MinialPublisher::time_callback,this));  
>>  }
>> ```
>> ### time_callback是这里的关键，每次触发都会发布一次话题消息。mssage中保存的字符串是Hello world加一个记数值
>> ### 然后通过RCLCPP——INFO宏函数打印一次日志信息，再通过发布者的publish方法将消息发布出去。
>> ```cpp
>> private:
>>  void time_callback()
>>  {
>>      auto message = std_msgs::msg::String();
>>      message.data = "Hello world!"+std::to_string(count_++);
>>      RCLCPP_INFO(this->get_logger(),"Publishing: '%s'",message.data.c_str());
>>      publisher_->publish(message); 
>>  }
>> ```
>> ### 最后是计时器、发布者和技术器字段的声明。
>> ```cpp
>> rclcpp::TimerBase::SharedPtrtimer_;
>> rclcpp::Publisher<std_msgs::msg::String>::SharedPtr publisher_;
>> size_t count_;
>> ```
>> ### 定义完节点类后还需要编写main函数，先初始化ROS2节点，然后使用rclcpp::spin创建MinimalPublisher,并且进入自选锁，当退出锁时，就会关闭节点结束了。完成以上发布者的代码后功能包还有一些内容需要设置。
---
> ## 设置发布者节点依赖项
>> ### 打开功能包的package.xml文件，根据前面的教程，先把<description>,
>> ### <maintainer>和<license>这些基础信息填写好了：
>> ```xml
>> <description>Examples of minimal publisher/subscriber using rclcpp</description>
>> <maintainer email="you@email.com">Your Name</maintainer>
>> <license>Apache License 2.0</license>
>> ```
>> ### 如还需添加的依赖项放到ament_cmake下边：
>> ```xml
>> <buildtool_depend>rclcpp</buildtool_depend>
>> <buildtool_depend>std_msgs</buildtool_depend>
>> ```
---
>> ## 设置发布者节点编译规则
>> ### 也就是编写CMakeLists.txt文件，在find_package语句下，新加入两行
>> ### 这里考虑它生成的那个CMake文件，并不是只有这几行代码
>> ```cmake
>> find_package(rclcpp REQUIRED)
>> find_package(std_msgs REQUIRED)
>> ```
>> ### 然后再设置具体的编译规则：
>> ```cmake
>> add_executable(talker src/publish_member_function.cpp)
>> ament_target_dependencies(talker rclcpp std_msgs)
>> ```
>> ### 最后要设置安装的规则，这样`ros2 run`命令才能找到可执行文件
>> ### `install`这个命令是将需要的文件放置在install里，其他编译产生的文件放在build里
>> ```cmake
>> install(TARGETS
>> talker
>> DESTINATION lib/${PROJECT_NAME})
>> ```
---
> ## 5.编写订阅者
>> ### 创建订阅者节点的代码
>> ### 创建订阅者cpp
>> ### 订阅者与发布者类似 
>>
>>
>>
>>
>>
>>
>
>