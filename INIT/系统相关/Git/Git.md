# Git
 > ```sudo apt install git```
## github是一个社区，git是一个服务系统，github只支持git分布式系统，所以故名github
## 工作就是创建和保存您项目的快照及与之后的快照进行对比
## 常用六大命令
### 上六.`git pull`
### 六五.`git checkout`
### 六四.`git add`  -> 暂存区的目录树被更新，同时工作区修改(或新增)的文件内容被写入到对象库中的一个新的对象中，而该对象的ID被记录在暂存区的索引中。
### 六三.`git commit`->暂存区的目录树写到版本库(对象)中，master分支会做相应的更新。即master指向的目录树就是提交时暂存区的目录树。
### 六二.`git push`
### 初六.`git clone`
### 
## 基本名称
## 
### workspace|工作区:->就是你在电脑里能看到的目录
### staging area|暂存区/缓存区:->英文叫stage或index。一般存放在“.git”目录下的index文件(.git/index)中所以我们把暂存区有时也叫索引(index)。
### local repository|版本库或本地仓库:->工作区有一个隐藏目录“.git”,这个不算工作区，而是Git的版本库
### remote repository|远程仓库:->github，gitlab
> #### 
## 一个简单的操作步骤
```bash
 git init   #初始化仓库
 git add .  #添加文件到缓存
 git commit #将暂存区添加到仓库
```
# Git有三种状态
## [已修改|modified]表示修改了文件，但还没将修改的结果放到暂存区
## [已暂存|staged]表示对已修改文件的当前版本做了标记，使之包含到下次提交的列表中
## [已提交|committed]表示文件已经安全的保存在本地的Git仓库
---
> ![src](基本操作示意.png)
---
> ####  工作区的文件被修改了，但还没有放到暂存区，就是已修改状态。如果文件已修改并放暂存区，就属于已暂存状态。如果Git仓库中保持着特定版本的文件，就属于已提交状态。
> #### 当执行```git reset HEAD```命令时暂存区的目录会被重写,被master分支指向的目录所替换，但是工作区不受影响。
> #### 当执行`git rm --cached<file>`命令时，会直接从暂存区删除文件，工作区则不做出改变。
> #### 当执行`git checkout`或者`git checkout --<file>`命令时，会用暂存区全部或指定文件替换工作区的文件，这个操作很危险，会清除工作区中未添加到暂存区的改动。
> #### 当执行`git check HEAD`或者`git checkout HEAD<file>`命令時，會用HEAD指向master分支中的全部或者部分文件替換暫存區和以及工作區中的文件。這個命令更加危險，不仅会覆盖工作区未提交的改动，也会清除缓存区的中未提交的改动

---
> ![dest](本地操作示意.png)
---

> ### `git commit -m"A"`参数-a把没有暂存的文件也一并提交了
> ### 提交到本地仓库
>> #### `git reset hand~ --soft`
>> ##### 取消上次提交，但不能是第一次
> ### `git status`
> ### 红色是已修改没有暂存，绿色是已暂存，然后commit提交，他们就到本地仓库了，再`git status`就看不到了。






---
> ### `git diff`
> ### `git log`
> ### 查看提交历史，commit后面的乱码就是这次提交的哈希值(唯一的)，参数-graph，更直观的看提交记录
> ### `git log --pretty=oneline`每一次的提交的信息变成一行
> ### `git log --pretty=format:"%h-%an,%ar:%S"`-----%h简化哈希 %an作者名字 %ar修订日期(距今) %ad修订日期 %s提交说明






> ---
> ### `git remote add orgin https://gitlab.com/qbykzyl/qiprogknow.git`
> ### 添加远程仓库，这只是一个例子，不要尝试，把我仓库覆盖了。orgin 是远程仓库的名字，后面是远程仓库的地址
> ### `git remote`可以查看远程仓库的名字
> ### 修改远程仓库的名字`git remote rename origin origin`
> ### `git push origin master`
> ### 将本地仓库的master仓库推送到远程仓库origin




---
> # 分支
> ## 每次提交生成一个新的版本时都会生成一个新版本的提交对象，每个提交对象都有一个哈希值，分支就是包含一个哈希值的文件，也可理解为指向一个提交对象的指针。
> ## master feature develop release hot fixes 
> ## origin/main 远程仓库上的分支
> ### `git branch --list`查看分支
> ### 创建分支`git branch feature`
> ### 切换分支`git checkout feature`参数-b新建一个分支并切换到该分支上
> ### `git merge feature`把feature分支合并到master分支下,可能会有冲突问题，改一下就好了





---
> # 推送
> ## 把本地仓库的代码推送到远程仓库
> ## `git push -u origin master`远程仓库名和我的分支名`git push -u origin feature`
> ## `git fetch`
> ## `git checkout -b feature origin/master`正常写法
> ## `git checkout -track origin/master`连接远程分支





---
# 储藏
> ## `git stash`
> ## `git stash push`可以多次储存
> ## `git stash apply`恢复我门刚才存储的修改
> ## `cat 文件名`查看文件什么样
> ## `git stash list`来看我们存储的文件
> ## `git stash apply stash@{2}`恢复第几次保存的文件
> ## 目录不干净，有修改未存储或未提交，不让你切换，或是拉取文件是在报护你修改的文件。
> ## `git checkout --文件名`恢复到未修改的状态，慎用，无法找回
> ## `git stash drop stash@{0}`删除某一次存储，但会往前顺延，也就是后面的代替前面的编号





---
> # 重置
> ## head当前提交,head~上一次的提交,head~2倒数第二次的提交。
> ## -soft 表示只是撤销commit的提交。add的东西还在，-hard,全部撤销，回到上次提交前的状态，不是特别提倡



git branch --set-upstream-to=origin/main main


---
> # 变基
> ## `git rebase`
---
[git](git-scm.com/book/zh/v2)

###### 你也看到了一直以来自己的成长。
###### 不要害怕，你要成为你想成为的人。
###### 人的一切痛苦本质上都是对自己无能的愤怒。
###### 沉默的时光，努力得不到结果，是在扎根、沉淀、积累。
###### 知我所能，我所能者，尽善尽美。
###### 熬得过无人问津的寂寞，才配拥有诗和远方。
###### 暴风雨结束后，你不记得怎么活下来，但有一件事是明白的，当你穿过暴风雨的时候你已经不是原来的那个人了。


