## Eigen 
## 这是一个高层次的C++库，有效支持`线性代数`，`矩阵`和`失量运算`，`数值分析`及其相关的算法。
### Eigen是一个开源库，从3.1.1版本开始遵从MPL2许可。
### OpenCV自带到Eigen的接口。
### 支持常用几何运算，包括旋转矩阵，四元数，矩阵变换，角轴等等
# 安装
### 在linux下的安装可以直接用命令安装：
> ## sudo apt-get install libeigen3-dev
### 源码安装 网上有很多教程
# CMakeLists.txt编写
### eigen库采用模板编程技术，仅用一些头文件组成，运行速度快。
## 正常：
> ```cmake
>  find_package(Eigen3 REQUIRED) 
>  include_directories(${EIGEN3_INCLUDE_DIR})
> ```
## 如果找不到头文件
> ## include_directories("/usr/include/eigen3") 
## 版本查看
### 终端输入命令：
> ### tac /usr/include/eigen/src/Core/util/Macros.h
---
# 使用
> ## Matrix类采用六个模板参数，这里了解前三个参数
>> ```cpp
>> Matrix<float,2,3> matrix23;//生成一个float类型的2*3的矩阵
>> matrix23<<1,2,3,1,2,3;//对矩阵进行初始化
>> //如果需要生成向量
>> Matrix<float,3,1> Vector3;//表示生成3行一列的向量
>> /*如果需要生成固定大小的矩阵*/
>> Matrix3f a;
>> MatrixXf b;
>> /*a是一个3*3动态大小的矩阵，具有已分配但当前未初始化的系数。
>> b是大小为0)0的动态大小矩阵，具有已分配但当前未初始化的系数*/
>> //对于以及确定长度的Matrix3f可以通过构造函数赋值
>> Matrix2d c(1,2,2,1);
>> ```
>> ### 如果不确定需要多大的矩阵或者是向量，可以用Dynamic动态申请大小：
>> ```cpp
>> Matrix<float,Dynamic,Dynamic> matrix;//表示生成一个float类型的n*n的矩阵
>> //如果需要生成向量
>> Matrix<float,Dynamic,1> Vectorn;//表示生成n行一列的向量
>> //如果不确定矩阵大小可以通过构造函数进行初始化。
>> MatrixXf matrx(10,29);//float类型的10*29的矩阵
>> VectorXf vector(30);//float类型的30长度的向量
>> ```
>> ### C++11,则可以通过传递任意数量的系数来初始化任意大小的固定大小的列或行向量
>> ```c++
>> Matrix<int,3,1> vector3;
>> vector3<<1,2,3;
>> ```
>> ### 获取矩阵的大小和矩阵的行列数：
>> ```cpp
>> cout<<"this matrix row"<<vector3.rows()<<"this matrix cols"<<vector3.cols()<<"this matrix size"<<vector3.size()<<endl;
>> ```
>> ### 当使用位置大小的Matrix时可以给另一个Matrix进行赋值，而且会改变另一个矩阵的大小。
>> ```cpp
>> MatrixXi matrix22(2,2);
>> MatrixXi matrix33(3,3);
>> matrix22=matrix33;
>> cout<<"matrix22:"<<matrix22.rows()<<"x"<<matrix22.cols()<<endl;
>> ```
>> ### Matrix的模板其实有6个参数
>> ```cpp
>> Matrix<typename Scalar,
>> int RowsAtCompileTime,
>> int ColsAtCompileTime,
>> int Options = 0,
>> int MaxRowsAtCompileTime = RowsAtCompileTime,
>> int MaxColsAtCompileTime = ColsAtCompileTime>
>> ```
>> ### Options是一个位字段。RowMajor，它指定这种类型的矩阵使用行优先存储顺序；默认情况下，存储顺序为列优先。
>> ## MaxRowsAtCompileTime并且MaxColsAtCompileTime当你要指定时很有用，可以编译时控制上限，避免动态内存分配。
> ## 矩阵运算
>> ### 简单的`+`，`-`，`*`，`/`,eigen对这些运算符进行了重载。
>> ```cpp
>> mat.transpose();//转置矩阵
>> mat.inverse();//逆矩阵
>> mat.conjugate();//共轭矩阵
>> mat.adjoint();//伴随矩阵
>> mat.trace();//矩阵的稚
>> mat.eigenvalues();//矩阵的特征值
>> mat.determinant();//矩阵求行列式的值
>> mat.diagonal();//矩阵对角线元素
>> mat.sum();//矩阵所有元素求和
>> mat.prod();//矩阵所有元素求积
>> mat.mean();//矩阵所有元素求平均
>> mat.maxCoeff();//矩阵最大值
>> mat.minCoeff();//矩阵的最小值
>> Matrix::Identity();//单位矩阵
>> dot();//点积
>> cross();//叉积
>> ```






















































