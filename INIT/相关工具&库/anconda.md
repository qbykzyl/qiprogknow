## 创建/删除环境
### 创建:
`conda create -n env_name python=3.7`
### 删除:
`conda env remove -n env_name`
## 
## 列出存在的环境
`conda env list`
## 
## 重命名环境
`create -n torch --clone pys`
> 把pys命名成torch
## 
## 进去/退出环境
`conda activate env_name`
`conda deactivate`
## 
## 安装环境
`conda install numpy =1.93`
`pip install numpy ==1.93`
`conda install－n env_name gatk`
## 
## 搜索
`conda search gatk`
`which gatk`
## 
## 查看自己安装的库
`conda list`
`conda list -n env_name`
## 
## 更新指定库
`conda updata gatk`
`conda updata` 
## 
## 删除环境中的某个库
`conda remove --name env_name gatk`
## 
## 卸载conda
```bash  
    、卸载 conda
    清理：rm -rf /opt/anaconda3\n
    删除 ~/.bash_profile中anaconda的环境变量
    删除Anaconda的可能存在隐藏的文件
    rm -rf  ~/.condarc  ~/.conda  ~/.continuum
    经过以上步骤后，Anaconda 就被彻底删除了
```
