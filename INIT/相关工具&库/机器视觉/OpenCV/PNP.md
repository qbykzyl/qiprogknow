# PNP
## 使用pnp方法可以计算相机在空间中的位姿，包括旋转R与平移t
### 简单来说，已知几个点在三维空间中坐标，未知相机的空间位姿。现在相机拍摄这几个已知点，得到图像，即已知点在图片中的像素坐标也已知了，通过已知点的三维坐标与二维坐标，相机参数，可以计算得到相机在世界坐标下的位姿R，t。
> # OpenCV中solvePnP函数
> ```cpp
> void solvePnP(InputArray objectPoints, InputArray imagePoints, InputArray cameraMatrix, InputArray distCoeffs, OutputArray rvec, OutputArray tvec, bool useExtrinsicGuess=false, int flags = CV_ITERATIVE)
> ```
> ### 输入：
>> ### objectPoints - 世界坐标系下的景物点的三维坐标
>> ### imagePoints  - 图像坐标系下对应点的二维坐标 
>> ### cameraMatrix - 相机的内参矩阵
>> ### distCoeffs   - 相机的畸变矩阵
>> ### flags        - 默认使用CV_ITERATIV 迭代法
> ### 输出：
>> ### rvec - 输出的旋转向量。使坐标点从世界坐标系旋转到相机坐标系
>> ### tvec - 输出的平移向量。使坐标点从世界坐标系平移到相机坐标系
>> ###
>> ###
>> ###
>> ###
>> ###
>> ###