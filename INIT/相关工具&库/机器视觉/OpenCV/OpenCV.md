## 头文件
`#include <opencv2/opencv.hpp>`
## 
## 命名空间
`using namespace cv;`
## 
## Mat 类型
`Mat (int rows,int cols,int type)`
### rows和cols合并成Size
## 
## type的类型为CV_8UC1,2,3(n)
### 控制创建n通道的矩阵
## 
## 读取视频流
### `VideoCapture`
---
### [trackbar相关](https://blog.csdn.net/huang9012/article/details/21811905?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168770063316800180618530%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168770063316800180618530&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~baidu_landing_v2~default-2-21811905-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=C%2B%2B%20opencv%20trackbar&spm=1018.2226.3001.4187)
>进度条相关
## 调整图像大小
---
### [图像尺寸调整1](https://blog.csdn.net/qq_28584889/article/details/102571013?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168775105916782425197741%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=168775105916782425197741&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v31_ecpm-11-102571013-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=opencv%E8%B0%83%E6%95%B4%E5%9B%BE%E5%83%8F%E5%A4%A7%E5%B0%8F&spm=1018.2226.3001.4187)
### [图像尺寸调整2](https://blog.csdn.net/qq_28584889/article/details/102571013?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168775105916782425197741%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=168775105916782425197741&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v31_ecpm-11-102571013-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=opencv%E8%B0%83%E6%95%B4%E5%9B%BE%E5%83%8F%E5%A4%A7%E5%B0%8F&spm=1018.2226.3001.4187)
## 调整窗口大小
### 主要函数： `cv::namedWindow(“window name”,flag)`
### `cv::namedWindow`功能为创建一个窗口，第一个参数设置窗口的名字，第二个参数指定窗口标识，一般默认为WINDOW_AUTOSIZE 。
### `WINDOW_AUTOSIZE`： 窗口大小自动适应图片大小，并且不可手动更改。
### `WINDOW_NORMAL`： 用户可以改变这个窗口大小
### `WINDOW_OPENGL`： 窗口创建的时候会支持OpenGL
### `resizeWindow("ControlPanel", 1200, 800);`调整窗口大小

---
--- 
## 画圆circle
### `circle(frame.frame, it, 1, Scalar(0, 255, 0), 2);`
### 参数分别为：画圆的图像，圆心，半径，颜色，轮廓厚度(整数为圆的厚度，负数则是一个需要填充的圆)，边界类型，位移。
## 在图像上写入文本
#### `putText(frame, “haha”, Point(tip_w, tip_h), HersheyFonts::FONT_HERSHEY_SIMPLEX, 3, cv::Scalar(0, 255, 0), 2);`
### 参数分别为：写文本的图像，需要绘制的字符串，文本位置，字体类型，字体大小，文本颜色，线条厚度，线条类，bottomLeftOrigin.
### [文章](https://blog.csdn.net/weixin_45842951/article/details/122201959?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168775795016800186513939%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=168775795016800186513939&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v31_ecpm-7-122201959-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=opencv%20circle&spm=1018.2226.3001.4187)
## 画线
### `line(frame.frame, pick_points[i - 1], pick_points[i], cv::Scalar(0, 255, 0), 2);`
### DDDD
### [文章](https://blog.csdn.net/AX642511931/article/details/124239421?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168775795016800222821080%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168775795016800222821080&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~baidu_landing_v2~default-4-124239421-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=opencv%20circle&spm=1018.2226.3001.4187)
---
---
















