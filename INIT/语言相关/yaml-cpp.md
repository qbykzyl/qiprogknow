## yaml是专门用来写配置文件的语言，非常简洁和强大，相比于json和xml格式要方便很多
### 该语言的设计初衷就是方便人类读写
### 实质是一种通用的数据串行化格式
---
> ## 安装yaml-cpp
>  ``` bash
> git clone https://github.com/jbeder/yaml-cpp.git
> cd yaml-cpp
> mkdir build && cd build
> cmake .. &&make -j
> sudo make install
> ```

---
## 基本语法
> ## `key: value` | key和value间有空格 | K: V
> ### 大小写敏感
> ### 使用缩进表示层级
> ### 缩进时不允许使用Tab键，只允许使用空格
> ### 缩进的空格数目不重要，只要相同层级的元素左侧对其即可
> ### 符号“#”表示注释，从这个字符一直到行未，都会被解析器忽略
> ### 字符串默认不使用引号表示，如果字符串之中包含空格或特殊字符，需要放在引号之中
> ---
> #### 单引号和双引号都可以使用，双引号不会对特殊字符串转义
> #### 单引号之中如果还有单引号，必须连续使用两个单引号转义
> #### 字符串可以写成多行，从第二行开始，必须有一个单空格缩进。换行符会被转成空格

---
> ## 支持的数据结构
> ### 对象：键值对的集合，又称为 映射(mapping)/哈希(hashes)/字典(dictionary)
> ### 数组：一组按次序排列的值，又称为序列(sequence)/列表(list)
> ### 纯量(scalars)：单个的、不可再分的值

---
> ## 常用解析库
> ### 一般常用yaml-cpp和OpenCV进行解析。
> ### 相比yaml-cpp，OpenCV的优点是可以在YAML文件中存储矩阵，读出来就是cv::Mat格式；
> ### 缺点是OpenCV要求YAML文件有一个特殊的头，与标准的YAML文件并不兼容。
> ### 可以理解为OpenCV定义了一种新的YAML格式。

---
> ## CMakeLists配置
> #### `find_package(yaml-cpp REQUIRED)`
> #### `include_directories(${YAML_CPP_INCLUDE_DIR})`
> #### `target_link_libraries(node_name yaml-cpp)`

---
> ## Node
> ## Node是yaml-cpp中的核心概念，是最重要的数据结构，它用于存储解析后的yaml信息。
> ## Node一共有以下几种type：
> ### Null 空节点
> ### Sequence 序列，类似于一个Vector，对应YAML格式中的对象
> ### Map,类似标准库中的Map，对应YAML格式中的对象
> ### Scalar标量，对应YAML格式中的常量
> ### 
---
> ## 生成Node的形式有很多种，loadFile()是最常见的一种：
> ### `Node LoadFile(const std::string& filename)`
> ### 其中`filename`就是yaml文件的路径
> ### 有了Node之后，所有的信息都可以检索到
> ### 比如`name`：
> ### ```cout<<"name"<<config["name"].as<string>()<<endl;```
> ### `as<string>()`表示将解析的内容转换成string类型，你也可以转换成其他类型，它是一个模板方法
> ### YAML::Node类提供了三个方法用于判断当前Node的类型:IsScalar()、IsSequence()、IsMap()
> ### 每实例化出一个Node对象之后都要首先判断一下Node对象的类型是什么，确定好类型之后再根据不同类型的特点进行对该Node对象的值操作。
> ### 如果对Node对象进行了不符合其类型特点的操作之后，代码会抛出异常。如果类型不确定，一定要加上try catch接受异常

## opencv读yaml文件
### 无法识别true和false
### 
























> ```cpp
>YAML::Node tmp_node_people = root_node["people"];
> 
>if (tmp_node_people.IsMap()) 
>{
>    //如果确定是Map，那么可以放心的使用[]操作符
>    YAML::Node tmp_node_name = tmp_node_people["name"];
>}
>else if (tmp_node_people.IsScalar())
>{
>    //如果确定是Scalar，可以放心的使用as方法
>    std::people_name = tmp_node_people.as<std::string>();
>    //但是需要注意的是，as<>中指定的类型如果是std::string不会有风险，因为一切都是字符串，转换没有问题，但是如果指定的是其他值，例如std::int，但实际yaml文件中该值是个字母，那么就会有异常抛出。因此，再使用as操作时，如果指定std::string之外的的类型时最好加上try catch。
>}
>else if (tmp_node_people.IsSequence())
>{
>    //如果确定是Sequence，那么可以放心的使用[]操作符，当然里面必须是整数格式的元素下标，否则会抛出异常。
>    YAML::Node tmp_node_name = tmp_node_people[0];
>}
>else
>{
>    //TODO:处理未知类型（未知类型是无法操作的）
>}
>```
---
> ## yaml文件解析
> ### 配置文件config.yaml
> ```yaml
> name: frank
> sex: male
> age: 18
> 
> skills: 
>  c++: 1
>  java: 1
>  android: 1
>  python: 1
> ```
> ---
> ### yaml-cpp相关API接口
> ```cpp
>#include <iostream>
>#include "yaml-cpp/yaml.h"
>#include <fstream>
> 
>using namespace std;
> 
>int main(int argc,char** argv)
>{
>    YAML::Node config;
>    try{
>         config = YAML::LoadFile("../config.yaml");
>    } 
>    catch(YAML::BadFile &e) {
>        std::cout<<"read error!"<<std::endl;
>        return -1;
>    }
>    
>    cout << "Node type " << config.Type() << endl;
>    cout << "skills type " << config["skills"].Type() << endl;
> 
>    //可以用string类型作为下表，读取参数
>    string age = "age";
>    cout << "age when string is label:" << config[age].as<int>() << endl;
> 
>    cout << "name:" << config["name"].as<string>() << endl;
>    cout << "sex:" << config["sex"].as<string>() << endl;
>    cout << "age:" << config["age"].as<int>() << endl;
> 
>    //读取不存在的node值，报YAML::TypedBadConversion异常
>    try
>    {
>        string label = config["label"].as<string>();
>    }
>    catch(YAML::TypedBadConversion<string> &e)
>    {
>        std::cout<<"label node is NULL"<<std::endl;
>    }
> 
>    cout << "skills c++:" << config["skills"]["c++"].as<int>() << endl;
>    cout << "skills java:" << config["skills"]["java"].as<int>() << endl;
>    cout << "skills android:" << config["skills"]["android"].as<int>() << endl;
>    cout << "skills python:" << config["skills"]["python"].as<int>() << endl;
> 
>    for(YAML::const_iterator it= config["skills"].begin(); it != config["skills"].end();++it)
>    {
>        cout << it->first.as<string>() << ":" << it->second.as<int>() << endl;
>    }
> 
>    YAML::Node test1 = YAML::Load("[1,2,3,4]");
>    cout << " Type: " << test1.Type() << endl;
> 
>    YAML::Node test2 = YAML::Load("1");
>    cout << " Type: " << test2.Type() << endl;
> 
>    YAML::Node test3 = YAML::Load("{'id':1,'degree':'senior'}");
>    cout << " Type: " << test3.Type() << endl;
> 	
>    // 保存config为yaml文件
>    ofstream ofs("./testconfig.yaml");
>    config["score"] = 99;
>    ofs << config;
>    ofs.close();
>    return 0;
>}
>
> ``` 
> # [更多内容](https://blog.csdn.net/Erice_s/article/details/130135530?ops_request_misc=&request_id=&biz_id=102&utm_term=yaml-cpp%E5%AE%89%E8%A3%85%E5%92%8C%E4%BD%BF%E7%94%A8&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-5-130135530.nonecase&spm=1018.2226.3001.4187)
---
---
---
---
---
---
---