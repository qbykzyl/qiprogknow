#Radar2023
## Logger
---
### access函数
#### 判断文件是否存在，并判断文件是否可写入。
#### linux下
##### 头文件,函数如下：
`<unistd.h>` `access`
>标准C++中该函数为`_access`头文件`io.h`,两者稍有不同。
### [access函数](https://blog.csdn.net/qq_28098067/article/details/50586810?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168769086016800182188494%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168769086016800182188494&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~baidu_landing_v2~default-1-50586810-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=c%2B%2B%20access%E5%87%BD%E6%95%B0&spm=1018.2226.3001.4187)
>判断文件写入属性
---
### [localtime函数](https://blog.csdn.net/Michaelwubo/article/details/41119945?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168769318616800182792978%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168769318616800182792978&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-2-41119945-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=localtime%E5%87%BD%E6%95%B0&spm=1018.2226.3001.4187) 
>获取当前时间的函数
---
### [strftime函数](https://blog.csdn.net/weixin_33835103/article/details/93753940?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168769630716800182159048%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168769630716800182159048&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~baidu_landing_v2~default-1-93753940-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=C%2B%2B%20strftime&spm=1018.2226.3001.4187)
>把时间格式化成需要的格式
---
### [strcat函数](https://blog.csdn.net/m0_64318128/article/details/124865165?ops_request_misc=&request_id=&biz_id=102&utm_term=strcat%E5%87%BD%E6%95%B0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-1-124865165.142^v88^insert_down38v5,239^v2^insert_chatgpt&spm=1018.2226.3001.4187)
>拼接字符串的函数
---
### [strcpy函数](https://blog.csdn.net/weixin_47970952/article/details/126163736?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168769418916800185840627%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168769418916800185840627&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-2-126163736-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=strcpy%E5%87%BD%E6%95%B0&spm=1018.2226.3001.4187)
>后面字符串复制到前面
---
### [auto关键字](https://blog.csdn.net/weixin_43744293/article/details/117440727?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168769747416800222896618%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168769747416800222896618&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-117440727-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=C%2B%2B%20auto&spm=1018.2226.3001.4187)
>自动判断变量类型，用于代替冗长的声明，或是接受未知类型的数据
---
### [spdlog类(1)](https://blog.csdn.net/gls_nuaa/article/details/126738472?ops_request_misc=&request_id=&biz_id=102&utm_term=spdlog&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-0-126738472.nonecase&spm=1018.2226.3001.4187) 
### [spdlog类(2)](https://blog.csdn.net/haojie_superstar/article/details/89383433?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168769902116800185877428%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168769902116800185877428&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-89383433-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=spdlog&spm=1018.2226.3001.4187)
>日志类
---
## Radar
### Radar类
#### spin旋转独占CPU time的进程，程序的主体。
#### init开始，初始化
---
### [try-catch用法](https://blog.csdn.net/feng19870412/article/details/128977250?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168775319616782427461087%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168775319616782427461087&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_click~default-2-128977250-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=c%2B%2B%20try%20catch%E7%94%A8%E6%B3%95&spm=1018.2226.3001.4187)
---
### [Map类](https://blog.csdn.net/weixin_55051736/article/details/129166032?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168775511416800182787184%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168775511416800182787184&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~baidu_landing_v2~default-1-129166032-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=c%2B%2B%20Map%E7%B1%BB&spm=1018.2226.3001.4187)
---
### [常用容器与相关函数](https://blog.csdn.net/wzh1378008099/article/details/105953273?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168775570216800211510946%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=168775570216800211510946&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v31_ecpm-5-105953273-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=C%2B%2B%E5%B8%B8%E7%94%A8%E5%AE%B9%E5%99%A8&spm=1018.2226.3001.4187)
---
### [迭代器是什么](https://so.csdn.net/so/search?q=c%2B%2B%20%E8%BF%AD%E4%BB%A3%E5%99%A8&t=&u=&urw=)
---
### [emplace_back函数](https://blog.csdn.net/weixin_45880571/article/details/119450328?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168775670616782425177587%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168775670616782425177587&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~baidu_landing_v2~default-1-119450328-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=C%2B%2B11%20emplace_back&spm=1018.2226.3001.4187)
---
### [return作用](https://blog.csdn.net/WebDestiny/article/details/100134388?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168777120616800182770148%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168777120616800182770148&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-1-100134388-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=return&spm=1018.2226.3001.4187)
---
### [标志位](https://so.csdn.net/so/search?q=%E6%A0%87%E5%BF%97%E4%BD%8D&t=&u=&urw=)
> 看看挺好
---
### [opencv坐标系转换](https://blog.csdn.net/panpan_jiang1/article/details/86063152?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168777361716800197065315%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=168777361716800197065315&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v31_ecpm-5-86063152-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=opencv%20projectpoint%E5%87%BD%E6%95%B0&spm=1018.2226.3001.4187)
---
---
### [remap重映射](https://so.csdn.net/so/search?q=opencv%20remap&t=&u=&urw=)

---
### initUndistortRectifyMap函数，用于计算无畸变和修正转换关系
#### [原文](https://blog.csdn.net/weixin_44279335/article/details/109448583?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168777924216800180634383%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168777924216800180634383&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-2-109448583-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=opencv%20initUndistortRectifyMap&spm=1018.2226.3001.4187)
---
---


