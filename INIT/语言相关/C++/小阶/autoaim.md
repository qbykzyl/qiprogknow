# 自瞄相关
> ## `std::make_shared<>`
> ### C++引入了智能指针，它会返回一个指定类型的std::shared_ptr。
> ### make_shared初始化的优点
> #### 提高性能
---
> ## `RCLCPP_COMPONENTS_REGISTER_NODE()`
> ### 节点注册
---
> ## `share_ptr<>`
> ### C++11引入的智能指针，特点是它所指向的资源具有共享性，即可以有多个share_ptr指向同一份资源。
---
> ## `unique_ptr<>`
> ### 该智能指针“拥有”它所指向的对象，与share_ptr<>不同，某个时刻只能有一个unique_ptr<>指向指定对象
---
> ## `rclcpp::Qos`
>> ### 这是一个用于配置ROS2发布者和订阅者质量保证的(Quality of Service,Qos)的类。
> ---
> #### `rm_qos_profile_t`
>> ##### 这是ROS2中定义Qos配置的数据结构。用于配置发布者和订阅者的通信结构，结构体。
> ##### `rmw_qos.depth` 
>> ###### 这是`rm_qos_profile_t`结构体中的一个字段，用于定义消息历史记录的策略中消息的最大数量
>> 
---
> ### 定义发布者
>> #### `rclcpp::Publisher<AutoaimMsg>::SharedPtr armor_msg_pub_;`
>> #### `armor_msg_pub_ = this->create_publisher<AutoaimMsg>("/armor_detector/armor_msg", qos);`
> ### 定义接收者
>> #### `rclcpp::Subscription<SerialMsg>::SharedPtr serial_msg_sub_;`
>> #### `serial_msg_sub_ = this->create_subscription<SerialMsg>("/serial_msg",qos,std::bind(&DetectorNode::sensorMsgCallback, this, _1));`
---
> ### `add_on_set_parameters_callback`
>> #### 是rclcpp库中的一个方法用于向ROS2节点添加自定义的参数回调函数，当节点参数被设置时，回调函数将被调用。
>> #### 该方法可用于在节点运行时对参数进行操作。
> ### 























