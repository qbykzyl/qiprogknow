# 基础语法
## 1标题
### 一级标题对应一个井号，二级标题对应两个井号，以此类推，一共六级标题
## 
## 2文本
### 斜体文本：可以在需要标注为斜体的文本开头，文本结尾，*输入一个星号*_或者一个下划线_
### 粗体文本：两个星号或者两个下划线_
### 粗斜体：三个
## 
## 3列表
### 无序列表，使用星号，加号，或是减号，再加一个空格作为列表标记
### 例
* 无序列表 1
+ 无序列表 2
- 无序列表 3
### 有序列表，使用数字加上点号，再加一个空格作为列表标记
### 例
1. 有序列表 1
2. 有序列表 2
3. 有序列表 3
### 控制列表层级，在符号前用Tab
- 无序列表
    - 无序列表
## 
## 4链接
### 中括号内写链接名称，括号内写链接地址
[笔记](https://gitlab.com/qbykzyl/qiprogknow.git)

<https://gitlab.com/qbykzyl/qiprogknow.git>
## 
## 5引用
### 引用的格式是在符号 > 后面书写文字，或者加一个空格再加文字，如下：
> 这是一个引用：
> 欢迎点赞
> 欢迎关注微信公众号沉潜吧
### 还可以嵌套
> 这是一个引用：
>> 欢迎点赞
>>> 欢迎关注微信公众号沉潜吧
## 
## 6分割线
### 可以在一行中三个 - 或者 * 来建立一个分割线，同时需要在分割线的上面空一行，如下：

---
【小生总醉平生】
 
或者

***
* * *
*****
- - -
## 
## 7删除线
### 删除线的使用，在需要删除的文字前后各使用两个 ~ ,如下：
~~这是要被删除的内容。~~
## 
## 8下划线
### 下划线的使用，在需要添加下划线的文字首部和尾部加上 <u> test </u>,如下：
 <u>这行文字已添加下划线</u>
 >这个命令云端好像不好用不好用
## 
## 9表格
### 表格使用 | 来分割不同的单元格，使用 - 来分割表头和其他行，如果想调整表格左对齐，右对齐，居中对齐：
 :- 将表头及单元格的内容左对齐；
 -: 左对齐
:-: 居中对齐
### 如下：
|姓名 |年龄  | 工作 |
|:---- |:---:| ---: |
|沈      |20    |牛逼|
| 岳     |20    |强大|
| 戚      |20   |不说了|
## 
## 10图片
### ！[图片描述](图片地址)
## 
## 脚注
### 脚注是对文本的备注说明，脚注与链接的区别如下：
链接:[文字](链接)
脚注:[文字](脚注)
## 
## 11代码块
### 如果在一个行内需要引用代码，只需要反引号引起来
Use the `printf()`function.
### 如果是一块，用三个反引号，第一行后面表示代码块使用的语言
```cpp
#include<iostream>
int main()
{
printf("HelloWorld");
}
```
### 支持以下语言
```bash
bash
c，clojure，cpp，cs，css
dart，dockerfile, diff
erlang
go，gradle，groovy
haskell
java，javascript，json，julia
kotlin
lisp，lua
makefile，markdown，matlab
objectivec
perl，php，python
r，ruby，rust
scala，shell，sql，swift
tex，typescript
verilog，vhdl
xml
yaml
```
# 基础语法到此为止，各平台的语法可能不一样
> 这是我学习的笔记仅供参考:
> 看原文请去这 
[原文](https://blog.csdn.net/qq_40818172/article/details/126260661?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522168718100116800182181313%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=168718100116800182181313&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-2-126260661-null-null.142^v88^insert_down38v5,239^v2^insert_chatgpt&utm_term=Markdown&spm=1018.2226.3001.4187)



## 行内式和独立式
$x+y=1$

$$x+y=1$$

## 上下标
$$
x_1+x^2=x^{x_1=y}\\
x^2
$$
## 括号
$$
f(x,y)=x^2=y_2,x \epsilon[1,10],y \epsilon \{1,2,3\}\\
\left(\sqrt {1\over 2}\right)
\left.(\sqrt {1\over 2}\right)
$$
## 省略号
$$
 {1 \over 2},{1\over 3},\ldots,{1\over 4}\\
 {1 \over 2},{1\over 3},\cdots,
$$
## 分数
$$
\frac {1-x}{y+1}\\
$$
$$
x\over {x+1}
$$
## 开方
$$
\sqrt[3]{9}
$$
## 向量
$$
\vec a \\
$$
$$
\vec {AB}
$$

## 极限
$$
\lim_{n\rightarrow+\infty}\frac{1}{n}
$$
## 求导
$$
y \prime=nx^{n-1}
$$
## 方程组
$$
y:\begin{cases}x+y=1\\
x-y=0 \end{cases}
$$
## 矩阵
$$
A = \left[\begin{matrix}
1&2&3\\
4&5&6\\
7&8&9\\
\end{matrix}\right]
$$
## 对数符号
$$
\log
\lg
\ln
$$
## 数字符号
$$
\not= \\
\approx \\
$$

$$
\leq \\
\gep \\
$$