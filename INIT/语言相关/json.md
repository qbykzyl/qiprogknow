## JSON
## 全称是JavaScript Object
## 轻量级、基于文本的(Text_based)、可读的(Human-Readable)格式。
## 不只能用于JavaScript语言
> # 语法
>> ## JSON的语法很简单，总结为下：
>> ### 数组(Array)用方括号("`[]`")表示
>> ### 对象(Object)用大括号("`{}`")表示
>> ### 名称/值对(`name/value`)组合成数组和对象
>> ### 名称name置于双引号中，值(`value`)有字符串、数值、布尔值、null、对象和数组
>> ### 并列的数据之间用逗号("`,`")分隔
>>```json
>> "name": "xdr630"
>> "favorite": "programming"
>>```
> ## [原文](https://blog.csdn.net/qq_58168493/article/details/122537407)