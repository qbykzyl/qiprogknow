# 文件名CMakeLists.txt
# [官方网站](https://cmake.org/)

## 大小写随意
### 
> ### 设置CMake最小版本
> `cmake_minimum_required(VERSION 3.10)`
---
> ### 设置项目名，（支持语言，版本号）
> `project(Tutorial C CXX)`
> `project(Tutorial VERSION 1.0)`
---
> `find_package()`
> ## 寻找相关包
> ### 该命令首先会在模块路径中寻找Find<name>.cmake，这是查找库的一个典型方法
> ### 具体查找路径依次为CMake：变量`${CMAKE_MODULE_PATH}`中的所有目录，
> ### 没有查询到，随后会查看它自己的模块目录/share/cmake-x.y/modules/;
> ### 如果没有找到这样的文件，命令会在～/.cmake/packages/或/usr/local/share/中的各个包目录中查找。
> ### 寻找<库名字的大写>Config.cmake，或者<库名字小写>-config.cmake
> ### (如`FIND_PACKAGE(Opencv REQUIRED)`,它会查找/usr/local/share/OpenCV中的OpenCVConfig.cmake或opencv-config.cmake)
---
> `SET(SRC_DIR_PATH src)`
> ### SET命令是用来设置变量，换句话说就是给变量赋值
> ### 本条命令就是SRC——DIR——PATH=src(src代表CMakelists的相对路径，即目录内的src文件夹，SRC_DIR_PATH表示源文件目录路径)
---
> `FILE(GLOB_RECURSE SOURCE_FILES "src/*.cpp")`
> ### FILE文件命令具体包括许多，在这里使用GOLB_RECURSE代表把该目录(src下)的所有源文件都赋予变量SOURCE_FILES
---
> `INCLUDE_DIRECTORIES(xxx/include)`
> ### 用来设置头文件，设置头文件的搜索目录，用以告诉CMake编译时去哪里寻找头文件
---
> `LINK_DIRECTORIES(xxx/lib)`
> ### 链接库，lib是库，该命令就是要链接库，设置动态链接库的目录
> ### 
>
>
---
> #### 然后配置一个文件已将版本号传递给源代码
> `configure_file(TutorialConfig.h.in TutorialConfig.h)`
--- 
> ### 生成可执行文件
> ### add the executable
> `add_executable(Tutorial tutorial.cpp)`
> ###### 后面是文件目录（路径），可以多个
> ###
---
> `TARGET_LINK_LIBRARIES(Tutorial ${LIB_PROJECT})` 
> ### 要链接动态库，参数指明库的名字即可，加上lib前缀和so后缀，或者以变量形式表示，如上条设置名称时不需要加上前后缀，第一个参数要与生成的可执行文件名字相同。
---
> `message(STATUS"hello world")`
> ### 该命令就是向终端窗口发送消息，本命令就是发送hello world
---
> ### 配置文件
> ###### 由于配置文件将写构建目录中，因此我们必须将该目录添加到路径列表中以搜索包含的文件
> `target_include_directories(Tutorial PUBLIC ${PROJECT_BINARY_DIR})`
## 
> ## 构建
> ### 首先
> ### 打开终端输入
> ```bash
> mkdir build  
> cd build
> cmake ..
> make 
> ./target
> ```











